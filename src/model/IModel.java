package model;

import view.CamembertView;

import java.util.List;

public interface IModel {
    public int size();

    public float getValues(int i);

    public String getDescription(int i);

    public String getTitle(int i);

    public int total();

    public String getTitle();

    public String getUnit();

    public void addItem(Item item);

    public List<Item> getItems();

    public void removeItem(Item item);

}
