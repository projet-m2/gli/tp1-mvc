package model;

import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

public abstract class ModelAdapter extends Observable implements IModel {

    Set<Observer> observers = new HashSet<Observer>();

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void notifyObservers(Object arg) {
        for(Observer observer: observers)
            observer.update(this, arg);
    }
}
