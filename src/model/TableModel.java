package model;

import javax.swing.table.AbstractTableModel;

public class TableModel extends AbstractTableModel {

    private ModelAdapter model;

    private final int NB_COLLUMN = 3;

    public TableModel(ModelAdapter model){
        this.model = model;
    }

    @Override
    public int getRowCount() {
        return model.size();
    }

    @Override
    public int getColumnCount() {
        return NB_COLLUMN;
    }

    @Override
    public Object getValueAt(int row, int collumn) {
        if(collumn == 0){
            return model.getTitle(row);
        }
        else if(collumn == 1){
            return model.getDescription(row);
        }
        else if(collumn == 2) {
            return model.getValues(row);
        }
        return null;
    }

}
