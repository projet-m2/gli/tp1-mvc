package model;

import java.util.ArrayList;
import java.util.List;

public class CamembertModel extends ModelAdapter {
    private List<Item> items;

    public CamembertModel() {
        items = new ArrayList<Item>();
    }

    private boolean isIndexOutOfBound(int index) {
        return index >= items.size() || index < 0;
    }

    public int size() {
        return items.size();
    }

    public float getValues(int index) {
        float value = 0;
        if (!isIndexOutOfBound(index)) {
            value = items.get(index).getBudget();
        }
        return value;
    }

    public String getTitle(int index) {
        String title = null;
        if (!isIndexOutOfBound(index)) {
            title = items.get(index).getTitle();
        }
        return title;
    }

    public String getDescription(int index) {
        String description = null;
        if (!isIndexOutOfBound(index)) {
            description = items.get(index).getDescription();
        }
        return description;
    }

    public int total() {
        int total = 0;
        for (Item i : items) {
            total += i.getBudget();
        }
        return total;
    }

    public String getTitle() {
        return "budget";
    }

    public String getUnit() {
        return "€";
    }

    public void addItem(Item item) {
        this.items.add(item);
    }

    public List<Item> getItems() {
        return this.items;
    }

    public void removeItem(Item item) {
        this.items.remove(item);
    }
}
