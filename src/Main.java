import controller.Controller;
import model.*;
import view.CamembertView;
import view.TableView;

import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main(String[] a) {
        JFrame window = new JFrame();
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setExtendedState(JFrame.MAXIMIZED_BOTH);

        // Create an instance of the model
        ModelAdapter model = new CamembertModel();

        Item jeux = new Item("Jeux vidéos", "Dépense jeux vidéos", 50);
        Item repas = new Item("Repas", "Dépense repas", 180);
        Item vacances = new Item("Vacances", "Besoin de vacances !!", 600);
        Item sport = new Item("Sport", "Le sport c'est la vie", 99);
        Item chocolat = new Item("Chocolat", "Hmm", 3);

        model.addItem(jeux);
        model.addItem(repas);
        model.addItem(vacances);
        model.addItem(sport);
        model.addItem(chocolat);

        // Maybe put some data in the model

        int oldFirst = 0;
        int oldLast = 0;

        CamembertView camembertView = new CamembertView(model);

        TableModel tableModel = new TableModel(model);
        TableView tableView = new TableView(tableModel);

        // Create the controller and link the controller to the model...
        Controller controller = new Controller(camembertView, tableView, model);
        camembertView.setController(controller);
        camembertView.addMouseListener(controller);

        tableView.addMouseListener(controller);




        // display layout
        GridLayout layout = new GridLayout(1, 2);

        window.getContentPane().add(controller.getView());
        window.getContentPane().add(tableView);

        window.setLayout(layout);
//		window.pack();
        window.setVisible(true);
        // window.pack();
    }
}
