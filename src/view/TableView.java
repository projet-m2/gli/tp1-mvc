package view;

import controller.Controller;
import model.ModelAdapter;
import model.TableModel;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

public class TableView extends JTable implements Observer {

    TableModel tableModel;

    public TableView(TableModel tableModel) {
        this.tableModel = tableModel;
        setModel(tableModel);
    }

    @Override
    public void update(Observable observable, Object o) {
    }
}
