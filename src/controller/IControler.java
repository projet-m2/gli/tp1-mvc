package controller;

import java.awt.*;

public interface IControler  {
    public void setSelected(boolean b);

    public int getSelectedPie();

    public void setSelectedPie(int i);

    public boolean isSelected();

    public void deSelect();

    public void selectPie(int i);

    public void nextPie();

    public void previousPie();

    public Component getView();

}
