package controller;

import model.IModel;
import view.CamembertView;
import view.TableView;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Arc2D;
import java.util.ArrayList;
import java.util.Collection;

public class Controller implements IControler, MouseListener, MouseMotionListener {

    private CamembertView camembertview;
    private TableView tableView;
    private IModel model;
    private boolean selected;
    private int selectedPie;

    public Controller(CamembertView camembertView, TableView tableView, IModel model) {
        this.camembertview = camembertView;
        this.tableView = tableView;
        this.model = model;
        this.selectedPie = 0;
    }

    @Override
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public int getSelectedPie() {
        return selectedPie;
    }

    @Override
    public void setSelectedPie(int selectedPie) {
        this.selectedPie = selectedPie;
    }

    @Override
    public boolean isSelected() {
        return selected;
    }

    @Override
    public void deSelect() {
        setSelected(false);
        camembertview.paint();
        tableView.clearSelection();
    }

    @Override
    public void selectPie(int selectedPie) {
        setSelected(true);
        setSelectedPie(selectedPie);
        System.out.println("Selected pie" + selectedPie);
        camembertview.paint();
        tableView.setRowSelectionInterval(selectedPie, selectedPie);
    }

    @Override
    public void nextPie() {
        setSelectedPie((getSelectedPie() + 1)
                % model.size());
        System.out.println("Selected pie" + getSelectedPie());
        camembertview.paint();
        tableView.setRowSelectionInterval(selectedPie, selectedPie);
    }

    @Override
    public void previousPie() {
        setSelectedPie(((getSelectedPie() - 1)+ model.size())
                % model.size());
        System.out.println("Selected pie" + getSelectedPie());
        camembertview.paint();
        tableView.setRowSelectionInterval(selectedPie, selectedPie);
    }

    @Override
    public Component getView() {
        return camembertview;
    }

    public void selectPieWithRow() {
        selectPie(tableView.getSelectedRow());
    }

    @Override
    public void mouseClicked(MouseEvent arg0) {
        boolean selectionTable = true;
        if (camembertview.getCenter().contains(arg0.getX(), arg0.getY())) {
            deSelect();
        } else {
            Collection<Arc2D> arcs = camembertview.getArcs();
            ArrayList<Arc2D> arcsList = (ArrayList<Arc2D>) arcs;
            for (int i = 0; i < arcs.size(); i++) {
                if (arcsList.get(i).contains(arg0.getX(), arg0.getY())
                        && !camembertview.getEmptyCenter().contains(arg0.getX(), arg0.getY())) {
                    selectPie(i);
                    selectionTable = false;
                }
            }
        }

        if(tableView.getSelectedRow() >= 0 && selectionTable){
            selectPieWithRow();
        }

        if (camembertview.getPrevious().contains(arg0.getX(), arg0.getY())) {
            nextPie();
        }

        if (camembertview.getNext().contains(arg0.getX(), arg0.getY())) {
            previousPie();
        }
    }

    @Override
    public void mousePressed(MouseEvent arg0) {
        camembertview.setPrevPosX(arg0.getX());
        camembertview.setPrevPosY(arg0.getY());
    }

    @Override
    public void mouseReleased(MouseEvent arg0) {

    }

    @Override
    public void mouseEntered(MouseEvent arg0) {

    }

    @Override
    public void mouseExited(MouseEvent arg0) {

    }

    @Override
    public void mouseDragged(MouseEvent arg0) {
        // difference in x from center:
        double dx = camembertview.getPieCenter().getX() - arg0.getX();
        double dy = camembertview.getPieCenter().getY() - arg0.getY();
        double angle1 = Math.atan2(dy, dx) / Math.PI * 180;

        dx = camembertview.getPieCenter().getX() - camembertview.getPrevPosX();
        dy = camembertview.getPieCenter().getY() - camembertview.getPrevPosY();
        double angle2 = Math.atan2(dy, dx) / Math.PI * 180;

        camembertview.setStartingAngle(camembertview.getStartingAngle() + (angle2 - angle1));

        camembertview.setPrevPosX(arg0.getX());
        camembertview.setPrevPosY(arg0.getY());

        camembertview.repaint();

    }

    @Override
    public void mouseMoved(MouseEvent arg0) {

    }
}
